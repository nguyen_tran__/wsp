var AbstractModule = require("./../Abstracts/Abstracts")
var ModuleManager = require("./ModuleManager");
var VSRegistry = require("./VSRegistry")
var Stream = require("./../Abstracts/VS").Stream
var VS = require("./../Abstracts/VS").VS

// Require a configuration JSON object to perform initialization. This config is provided by user code
function init(configJSON, dataModelJSON){
	console.log("Hello world from Init.js");
	
	// Initializing virtual sensors
	initVS(configJSON, dataModelJSON);
	
	// Initialize Web Server
	initHTTP(configJSON);
}

function initVS(configJSON, dataModelJSON){
	var modelManager = ModuleManager.LoadModule(dataModelJSON["ModelManager"])
	
	var sensors = configJSON["sensors"];
	for(var i = 0; i < sensors.length; i++){
		var currSensor = sensors[i];
		
		// Initialize streams
		var streams = currSensor["streams"];
		var vsstreams = []
		for(var j = 0; j < streams.length; j++){
			// Initialize virtual stream object
			var currStream = streams[j];
			var streamInfo = modelManager.CreateStreamInfo(dataModelJSON["Infos"][currStream["id"]]);
			var stream = new Stream(currStream["id"], streamInfo);
			
			// Register stream with their plugin
			var currPluginInfo = currStream["plugin"];
			var plg = ModuleManager.LoadModule(currPluginInfo, {ModelManager : modelManager});
			plg.RegisterDatastream(stream, currPluginInfo["params"]);
			
			// Push virtual stream into the list
			vsstreams.push(stream);
		}
		
		// Initialize virtual sensor object
		var sensorInfo = modelManager.CreateSensorInfo(dataModelJSON["Infos"][currSensor["id"]]);
		var vs = new VS(currSensor["id"], sensorInfo, vsstreams);
		
		// Add virtual sensor to the sensor registry
		VSRegistry.addVS(vs);
		// for(var k = 0; k < vs.streams.length; k++){
			// console.log(vs.streams[k]);
		// }
	}
}

function initHTTP(configJSON){
	httpServerModule = ModuleManager.LoadModule(configJSON.http, {"sensorRegistry" : VSRegistry});
	httpServerModule.StartHTTPServer();
}

module.exports.init = init