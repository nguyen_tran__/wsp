class VS {
	constructor(id, sensorInfo, streams){
		this.type = "VS"
		this.sensorID = id;
		this.sensorInfo = sensorInfo;
		this.streams = streams;
		
		// Aliases
		this.id = this.sensorID;
		this.Datastreams = this.streams;
		this.model = this.sensorInfo;
	}
}
module.exports.VS = VS

class Stream {
	constructor(id, streamInfo){
		this.type = "VSSTREAM";
		this.streamID = id;
		this.streamInfo = streamInfo;
		this.Observations = {};
		
		// Aliases
		this.id = this.streamID;
		this.model = this.streamInfo;
	}
	
	addObservation(newObs){
		this.Observations = newObs;
		// console.info("Stream " + this.streamID + " receives new observation: ");
		// console.info(this.obs);
	}
}
module.exports.Stream = Stream

class Observation {
	constructor(id, result, observationInfo){
		this.type = "OBSERVATION";
		this.observationID = id;
		this.result = result;
		this.observationInfo = observationInfo;
		
		// Aliases
		this.id = this.observationID
		this.model = this.observationInfo
	}
}
module.exports.Observation = Observation