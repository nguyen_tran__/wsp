var EventEmitter = require("events");

// Every module in WSP is capable of emitting and reacting to events.
class AbsModule extends EventEmitter{
	constructor(systemParams, moduleParams, subModules, type){
		// Check number of arguments before building the module 
		if(arguments.length !== 4){
			throw "" + arguments.length + " arguments are given. Number of parameters given to a module must be 4. "
		}
		
		super();
		this.systemParams = systemParams;
		this.moduleParams = moduleParams;
		this.subModules = subModules;
		this.type = type;
	}
	
	// All inputs to this method are STRING
	SetAttribute(attrName, fromParams, index){
		var attrValue = this[fromParams][index]
		if(!attrValue){
			throw "Attribute " + attrName + " cannot be found at index " + index + " of the params set " + fromParams;
		} else {
			this[attrName] = attrValue;
		}
	}
}
module.exports.AbsModule = AbsModule

// An abstract plugin that retrieve observations from data sources and add to a given stream
class AbsPlugin extends AbsModule{
	constructor(systemParams, moduleParams, subModules, type){
		super(systemParams, moduleParams, subModules, type)
		this.SetAttribute("ModelManager", "systemParams", "ModelManager")
		this.VS = require("./VS")
	}
	
	// Abstract function
	// Use the given params to interact with the sensor and get a new reading
	// ModelManager can be used to create observation in the appropriate format
	GetNewReading(params){
		throw "GetNewReading() method of this plugin is not implemented"
	}
	
	// Abstract function
	// Calls the GetNewReading() method under a certain condition to add a new observation to the stream
	RegisterStream(stream, params){
		throw "RegisterDatastream() method of this plugin is not implemented"
	}
}
module.exports.AbsPlugin = AbsPlugin


// An abstract plugin that poll sources continuously for new observations
class AbsPluginPolling extends AbsPlugin{
	constructor(systemParams, moduleParams, subModules){
		super(systemParams, moduleParams, subModules, "PLUGIN_POLLING");
	}
	
	RegisterDatastream(stream, params){
		// Check for the existence of period parameter
		var period = params.period;
		if(!period){
			throw "Period parameter for this polling plugin is not given.";
		}

		// Validate the given stream object
		if(stream.type !== "VSSTREAM"){
			throw "The given stream object for registering is invalid";
		}
		
		// Plugin get a new reading and emit an after a period of time specified in the given parameters
		var eventName = "NEW_READING_STREAM_" + stream.id;
		var interval = setInterval(function(plg, stream, params){
			var newReading = plg.GetNewReading(params);
			if(!newReading){
				clearInterval(interval);
				console.log("No more data for stream " + stream.streamID + ". Interval cleared");
			} else {
				plg.emit(eventName, stream, newReading)
			}
		}, period, this, stream, params, interval)
		
		// Update the observation in the stream when a new reading is observed
		this.on(eventName, function(stream, newReading){
			stream.addObservation(newReading);
		});
	}
}
module.exports.AbsPluginPolling = AbsPluginPolling


// An abstract module encapsulating utilities that generate description of sensors, streams and observations 
// according to a certain data model
class AbsModelManager extends AbsModule{
	constructor(systemParams, moduleParams, subModules){
		super(systemParams, moduleParams, subModules, "MODEL_MANAGER");
	}
	
	CreateSensorInfo(params){
		throw "CreateSensorInfo() method of this Model Manager is not implemented"
	}
	
	CreateStreamInfo(params){
		throw "CreateStreamInfo() method of this Model Manager is not implemented"
	}
	
	CreateObservationInfo(params){
		throw "CreateObservationInfo() method of this Model Manager is not implemented"
	}
}
module.exports.AbsModelManager = AbsModelManager;

// An abstract module representing a HTTP server in WSP
// Each subModule of HTTP module holds a router for a specific route
class AbsHTTPServer extends AbsModule{
	constructor(systemParams, moduleParams, subModules){
		super(systemParams, moduleParams, subModules, "HTTP_SERVER");
		this.SetAttribute("host", "moduleParams", "host");
		this.SetAttribute("port", "moduleParams", "port");
		this.SetAttribute("version", "moduleParams", "version");
		this.SetAttribute("registry", "systemParams", "sensorRegistry");
		
		// Provide sensor registry and server host information to the router
		for(var i = 0; i < this.subModules.length; i++){
			subModules[i]["registry"] =  this["registry"]
			subModules[i]["hostInfo"] = {
				"host" : this.host,
				"port" : this.port,
				"version" : this.version
			}
		}
	}
	
	StartHTTPServer(){
		throw "CreateHTTPServer() method of this HTTP Server module is not implemented"
	}
}
module.exports.AbsHTTPServer = AbsHTTPServer;

class AbsHTTPRouter extends AbsModule{
	constructor(systemParams, moduleParams, subModules){
		super(systemParams, moduleParams, subModules, "HTTP_ROUTER");
		this.SetAttribute("route", "moduleParams", "route");
		this.SetAttribute("verb", "moduleParams", "verb");
	}
	
	CreateRouter(){
		throw "CreateRouter() method of this HTTP Router module is not implemented"
	}
	
	InitRouter(){
		throw "InitRouter() method of this HTTP Router module is not implemented"
	}
	
	ExtractHTTPRequestParams(req){
		throw "ExtractHTTPRequestParams() method of this HTTP Router module is not implemented"
	}
	
	GenerateHTTPResponse(params){
		throw "GenerateHTTPResponse() method of this HTTP Router module is not implemented"
	}
	
	SendResponse(res, response){
		throw "SendResponse() method of this HTTP Router module is not implemented"
	}
}
module.exports.AbsHTTPRouter = AbsHTTPRouter;

class AbsView extends AbsModule{
	constructor(systemParams, moduleParams, subModules){
		super(systemParams, moduleParams, subModules, "VIEW");
	}
	
	CreateView(dataObject){
		throw "CreateView() method of this View module is not implemented"
	}
}
module.exports.AbsView = AbsView;
