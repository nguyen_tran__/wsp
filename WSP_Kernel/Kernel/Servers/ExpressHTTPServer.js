var express = require("express");
var Abstracts = require("./../Abstracts/Abstracts");

class HTTPServer extends Abstracts.AbsHTTPServer{
	constructor(systemParams, moduleParams, subModules){
		super(systemParams, moduleParams, subModules);
		this.httpServer = null;
		this.app = null;
	}
	
	StartHTTPServer(){
		this.app = express();
		var that = this;
		for(var i = 0; i < this.subModules.length; i++){
			this.app.use("/v" + this.version, this.subModules[i].CreateRouter());
		}
		
		this.httpServer = this.app.listen(this.port, function(){
			console.log("Start listening on port " + that.port);
		});
		
		return this.httpServer
	}
}
module.exports = HTTPServer