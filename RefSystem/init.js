var InitModule = require("./Kernel/SystemUtilities/Init")

// Get commandline args 
var args = process.argv.slice(2);
var configFolder = args[0]

configJSON = require(configFolder + "/configuration.json")
dataModelJSON = require(configFolder + "/data_model.json")
InitModule.init(configJSON, dataModelJSON);