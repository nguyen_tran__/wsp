var WebSocketServer = require("ws");
var UrlPattern = require('url-pattern');
var registry = require("./../registry/sensor_registry");

function initWS(server){
	var wss = new WebSocketServer.Server({"server": server});
	console.info('WebSocket Server started...');
	wss.on('connection', function(ws){
		var url = ws.upgradeReq.url;
		console.info(url);
		matchURL(url, ws);
	});
}

function matchURL(url, ws){
	var pattern = new UrlPattern("/sensors/:id");
	matches = pattern.match(url)
	if(!matches){
		ws.send("Unable to parse this request");
		ws.close();
	} else {
		sensor = registry.getSensor(matches.id);
		setInterval(function(ws, s){
			ws.send(JSON.stringify(sensor));
		}, 1000, ws, sensor);
	}
}

module.exports = initWS;

