var ExpressHTTPRouter = require("./../Kernel/Servers/ExpressHTTPRouter")

class RootRouter extends ExpressHTTPRouter{
	constructor(systemParams, moduleParams, subModules){
		super(systemParams, moduleParams, subModules);
	}
	
	ExtractHTTPRequestParams(req){
		return {
			"rootRoute" : "http://" + this.hostInfo.host + "::" + this.hostInfo.port + "/v" + this.hostInfo.version + "/"
		}
	}
	
	GenerateHTTPResponse(params){
		var entityTypes = ["Sensors", "Datastream", "Observations", "ObservedProperties", "FeaturesOfInterest"];
	
		var resJSON = {};
		resJSON["value"] = [];
		
		for(var i = 0; i < entityTypes.length; i++){
			resJSON.value.push({
					"name" : entityTypes[i],
					"url" : params.rootRoute + entityTypes[i]
				}
			);
		}
		
		console.info(resJSON);
		return resJSON;
	}
	
	SendResponse(res, response){
		res.send(response);
	}
}
module.exports = RootRouter;
