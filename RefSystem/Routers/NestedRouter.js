var ExpressHTTPRouter = require("./../Kernel/Servers/ExpressHTTPRouter")

class NestedRouter extends ExpressHTTPRouter{
	constructor(systemParams, moduleParams, subModules){
		super(systemParams, moduleParams, subModules);
		this.VALID_PROP_TYPE = {"Datastreams" : "", "ObservedProperties" : "", "FeaturesOfInterest" : "", "Observations" : ""};
	}
	
	ExtractHTTPRequestParams(req){
		return req.params;
	}
	
	GenerateHTTPResponse(params){
		var entityType = params.entityType;
		var entityID = params.id;
		var entityProp = params.prop;
		
		// Load data from registry
		var data = null;
		if(entityType === "Sensors"){
			if(!entityID){
				data = this.registry.getEntities("Sensors");
			} else {
				data = this.registry.getEntity("Sensors", entityID, entityProp);
			}
		} else if(entityType === "Datastreams" || entityType === "ObservedProperties" || entityType === "FeaturesOfInterest" || entityType === "Observations"){
			if(!entityID){
				data = this.registry.getEntities("Datastreams");
			} else {
				data = this.registry.getEntity("Datastreams", entityID, entityProp);
			}
		}
		
		// Generate JSON representation using the JSONview
		if(!data){
			return null;
		} else {
			if(this.VALID_PROP_TYPE.hasOwnProperty(entityProp)){
				entityType = entityProp
			}
			var resJSON = this.JSONView.CreateView(data, entityType);
			return resJSON;
		}
	}
	
	SendResponse(res, response){
		if(!response){
			res.status(400);
			res.send("Unknown resource type")
		} else {
			res.send(response)
		}
	}
}
module.exports = NestedRouter;
