AbsModules = require("./../Kernel/Abstracts/Abstracts")
VS = require("./../Kernel/Abstracts/VS")

class ReplayPlugin extends AbsModules.AbsPluginPolling {
	constructor(systemParams, moduleParams, subModules){
		super(systemParams, moduleParams, subModules);
		this.fileHandlers = {};
		this. lineByLine = require("n-readlines")
	}
	
	GetNewReading(params){
		function cleanString(str){
			return str.trim().replace(/(\n|\r)+$/, '')
		}
		
		var csvFolderPath = params.path;
		var streamID = params.streamID;
		var csvPath = csvFolderPath + "/" + streamID + ".csv";
		var fileHandler = null;
		
		if(this.fileHandlers.hasOwnProperty(streamID)){
			fileHandler = this.fileHandlers[streamID]
		} else {
			try{
				fileHandler = new this.lineByLine(csvPath)
			}
			catch(err){
				return null;
			}
			this.fileHandlers[streamID] = fileHandler
		}
		
		var line = fileHandler.next();
		if(!line){
			return null;
		}
		
		var time = cleanString(("" + line).split(" ")[0]);
		var result = cleanString(("" + line).split(" ")[1]);
		var obsID = streamID + "--Obs--" + time
		
		var obsInfo = this.ModelManager.CreateObservationInfo(obsID, time, time, result);
		var obs = new VS.Observation(obsID, result, obsInfo)
		return obs;

	}
}
module.exports = ReplayPlugin;