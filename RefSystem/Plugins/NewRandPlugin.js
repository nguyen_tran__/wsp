AbsModules = require("./../Kernel/Abstracts/Abstracts")
VS = require("./../Kernel/Abstracts/VS")


class RandPlugin extends AbsModules.AbsPluginPolling {
	constructor(systemParams, moduleParams, subModules){
		super(systemParams, moduleParams, subModules)
	}
	
	GetNewReading(params){
		var max = params.max;
		var min = params.min;
		var result = Math.random() * (max - min) + min;
		
		var streamID = params.streamID;
		var obsID = streamID + "--obs"
		var time = (new Date()).toISOString()
		var obsInfo = this.ModelManager.CreateObservationInfo(obsID, time, time, result);
		var obs = new VS.Observation(obsID, result, obsInfo)
		return obs;
	}
}
module.exports = RandPlugin;