exports.THING = "Thing";
exports.THINGS = "Things";

exports.LOCATION = "Location";
exports.LOCATIONS = "Locations";

exports.HISTORICAL_LOCATION = "HistoricalLocation";
exports.HISTORICAL_LOCATIONS = "HistoricalLocations";

exports.DATASTREAM = "Datastream";
exports.DATASTREAMS = "Datastreams";

exports.SENSOR = "Sensor";
exports.SENSORS = "Sensors";

exports.OBSERVED_PROPERTY = "ObservedProperty";
exports.OBSERVED_PROPERTIES = "ObservedProperties";

exports.OBSERVATION = "Observation";
exports.OBSERVATIONS = "Observations";

exports.FEATURE_OF_INTEREST = "FeatureOfInterest";
exports.FEATURES_OF_INTEREST = "FeaturesOfInterest";

exports.OM_CategoryObservation = "http://www.opengis.net/def/observationType/OGC-OM/2.0/OM_CategoryObservation";
exports.OM_CountObservation = "http://www.opengis.net/def/observationType/OGC-OM/2.0/OM_CountObservation";
exports.OM_Measurement = "http://www.opengis.net/def/observationType/OGC-OM/2.0/OM_Measurement";
exports.OM_Observation = "http://www.opengis.net/def/observationType/OGC-OM/2.0/OM_Observation";
exports.OM_TruthObservation = "http://www.opengis.net/def/observationType/OGC-OM/2.0/OM_TruthObservation";

var ENCODING_TYPE_PDF = "application/pdf";
var ENCODING_TYPE_SENSORML = "http://www.opengis.net/doc/IS/SensorML/2.0";
exports.getSensorMetadataEncoding = function(encodingType){
	if(encodingType == "PDF"){
		return ENCODING_TYPE_PDF;
	} else if(encodingType == "SensorML"){
		return ENCODING_TYPE_SENSORML
	} else {
		return null;
	}
}