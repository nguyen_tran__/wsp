var Generic = require("./Generic");
var ValueCode = require("./ValueCode");

class HistoricalLocations extends Generic {
	constructor(id, time){
		super(id, ValueCode.HISTORICAL_LOCATIONS, [ValueCode.THINGS, ValueCode.LOCATION]);
		this.time = time;
	}
	
	addThings(things){
		this.addEntities(ValueCode.THINGS, things);
	}
	
	addLocation(loc){
		this.addEntities(ValueCode.LOCATION, loc);
	}
};

function createHistoricalLocation(id, time){
	return new HistoricalLocations(id, time);
}

function createDummyHistoricalLocation(id){
	return createHistoricalLocation(id, "00:00:00 -- 00-00-0000");
}

module.exports = createHistoricalLocation;
createHistoricalLocation.dummy = createDummyHistoricalLocation;