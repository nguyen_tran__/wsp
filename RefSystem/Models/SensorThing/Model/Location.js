var Generic = require("./Generic");
var ValueCode = require("./ValueCode");

class Locations extends Generic {
	constructor(id, name, description, loc){
		super(id, ValueCode.LOCATIONS, [ValueCode.THINGS, ValueCode.HISTORICAL_LOCATIONS]);
		this.name = name;
		this.description = description;
		this.encodingType = "application/vnd.geo+json"
		this["location"] = loc
	}
	
	addHistoricalLocations(historicalLocations){
		this.addEntities(ValueCode.HISTORICAL_LOCATIONS, historicalLocations);
	}
	
	addThings(things){
		this.addEntities(ValueCode.THINGS, things);
	}
};

function createLocation(id, name, description, loc){
	loc = new Locations(id, name, description, loc);
	return loc;
}

function createDummyLocation(id){
	loc = new Locations(id, "Location " + id, "Description of location " + id, {"Key" : "Value"});
	return loc;
}

module.exports = createLocation;
createLocation.dummy = createDummyLocation;