//Dependencies
var Sensor = require("./Sensor");
var Datastream = require("./Datastream");
var ObservedProperty = require("./ObservedProperty");
var FeatureOfInterest = require("./FeatureOfInterest");
var Observation = require("./Observation");
var SensorRegistry = require("./../SensorRegistry/SensorRegistry");

function CreateSTModel(sensorInfo, streamsInfo){
	sensorModel = Sensor(sensorInfo);
	for(var i = 0; i < streamsInfo.length; i++){
		//Generate stream object
		stream = Datastream(streamsInfo[i]);
		
		//Create observed property. WSP do not manage the multiplicity of observed property.
		propInfo = streamsInfo[i].observedProperty;
		observedProperty = ObservedProperty(propInfo);
		
		//Create feature of interest. WSP do not manage the multiplicity of foi.
		foiInfo = streamsInfo[i].featureOfInterest;
		foi = FeatureOfInterest(foiInfo);
		
		//Add an empty observation to link datastream with feature of interest
		obs = Observation.dummy("obs_" + sensorInfo.id);
		
		//Link streams with property and obs
		stream.addObservedProperty(observedProperty);
		stream.pushObservation(obs);
		
		//Link observation with feature of interest
		obs.addFeatureOfInterest(foi);
		
		sensorModel.pushDatastream(stream)
	}
	return sensorModel;
}

function CreateObservation(result, datastream){
	oldObservation = datastream.Observations[0];
	newObservation = Observation(oldObservation["@iot.id"], "00:00:00 -- 00-00-0000", "00:00:00 -- 00-00-0000", result, "Quality", null, null);
	newObservation.addFeatureOfInterest(oldObservation.FeatureOfInterest);
	
	return newObservation;
}

module.exports.CreateSTModel = CreateSTModel;
module.exports.CreateObservation = CreateObservation;