var ValueCode = require("./ValueCode");
var Location = require("./Location");
var HistoricalLocation = require("./HistoricalLocation");
var Thing = require("./Thing");
var Datastream = require("./Datastream");
var Sensor = require("./Sensor");
var ObservedProperty = require("./ObservedProperty");
var FeatureOfInterest = require("./FeatureOfInterest");
var Observation = require("./Observation");

thing1 = Thing.dummy("th1");
loc1 = Location("loc 1", "New location 1", "This is a description", {"Key" : "Value"});
loc2 = Location("loc 2", "New location 2", "This is a description", {"Key" : "Value"});

histLoc1 = HistoricalLocation.dummy("hLoc 1");
histLoc2 = HistoricalLocation.dummy("hLoc 2");

thing1.addLocations([loc1, loc2]);
thing1.addHistoricalLocations([histLoc1, histLoc2]);

dt1 = Datastream.dummy("dts1");

sen1 = Sensor.dummy("sen1");

obs1 = ObservedProperty.dummy("Obs1");

foi1 = FeatureOfInterest.dummy("foi1");

obse1 = Observation.dummy("obse1");
console.info(obse1);