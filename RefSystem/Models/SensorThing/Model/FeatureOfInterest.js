var Generic = require("./Generic");
var ValueCode = require("./ValueCode");

class FeatureOfInterest extends Generic {
	constructor(id, name, description, feature){
		super(id, ValueCode.FEATURES_OF_INTEREST, [ValueCode.OBSERVATIONS]);
		this.name = name;
		this.description = description;
		this.encodingType = "application/vnd.geo+json";
		this.feature = feature;
	}
	
	addObservations(observations){
		this.addEntities(ValueCode.OBSERVATIONS, observations);
	}
	
	pushObservation(observation){
		this.pushEntity(ValueCode.OBSERVATIONS, observation);
	}
};

function createFeatureOfInterest(id, name, description, feature){
	foi = new FeatureOfInterest(id, name, description, feature);
	return foi;
}

function createFromInfoObject(foiInfo){
	foi = createFeatureOfInterest(foiInfo.id, foiInfo.name, foiInfo.description, foiInfo.feature);
	return foi;
}

function createDummyFeatureOfInterest(id){
	foi = new FeatureOfInterest(id, "Feature " + id, "Description of feature " + id, {"Key" : "Value"});
	return foi;
}

module.exports = createFromInfoObject;
createFeatureOfInterest.dummy = createDummyFeatureOfInterest;