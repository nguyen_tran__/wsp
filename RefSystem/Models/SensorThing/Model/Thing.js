var Generic = require("./Generic");
var ValueCode = require("./ValueCode");

class Things extends Generic {
	constructor(id, name, description, property){
		super(id, ValueCode.THINGS, [ValueCode.LOCATIONS, ValueCode.HISTORICAL_LOCATIONS]);
		this.name = name;
		this.description = description;
		this.property = property
	}
	
	addLocations(locations){
		this.addEntities(ValueCode.LOCATIONS, locations);
	}
	
	addHistoricalLocations(historicalLocations){
		this.addEntities(ValueCode.HISTORICAL_LOCATIONS, historicalLocations);
	}
};

function createThing(id, name, description, property){
	thing = new Things(id, name, description, property);
	return thing;
}

function createDummyThing(id){
	thing = new Things(id, "Thing " + id, "Description of thing " + id, {"Key" : "Value"});
	return thing;
}

module.exports = createThing;
createThing.dummy = createDummyThing;