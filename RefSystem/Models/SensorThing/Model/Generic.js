class Generic {
	constructor(id, fromEntity, toEntities){
		this["@iot.id"] = id;
		this.generateSelfLink("http://localhost:8080/", fromEntity, id);
		for(var i = 0; i < toEntities.length; i++){
			this.generateNavigationLink(fromEntity, id, toEntities[i]);
		}
	}
	
	generateNavigationLink(fromEntity, id, toEntity){
		var linkName = toEntity + "@iot.navigationLink";
		var url = fromEntity + "(" + id + ")/" + toEntity;
		this[linkName] = url;
	}
	
	generateSelfLink(host, entityType, id){
		this["@iot.selfLink"] = host + entityType + "(" + id + ")";
	}
	
	addEntities(toEntities, entities){
		if(Array.isArray(entities)){
			this[toEntities] = entities;
		} else {
			this[toEntities] = [entities];
		}
	}
	
	addEntity(toEntity, entity){
		if(!Array.isArray(entity)){
			this[toEntity] = entity;
		}
	}
	
	pushEntity(toEntities, entity){
		if(!Array.isArray(entity)){
			if(this.hasOwnProperty(toEntities)){
				this[toEntities].push(entity);
			} else {
				this.addEntities(toEntities, entity);
			}
		}
	}
}

module.exports = Generic;