var Generic = require("./Generic");
var ValueCode = require("./ValueCode");

class Datastreams extends Generic {
	constructor(id, name, description, unitOfMeasurement, observationType, observedArea, phenomenonTime, resultTime){
		super(id, ValueCode.DATASTREAMS, [ValueCode.SENSOR, ValueCode.THING, ValueCode.OBSERVATIONS, ValueCode.OBSERVED_PROPERTY]);
		this.name = name;
		this.description = description;
		this.unitOfMeasurement = unitOfMeasurement;
		this.observationType = observationType;
		this.observedArea = observedArea;
		this.phenomenonTime = phenomenonTime;
		this.resultTime = resultTime;
	}
	
	addThing(thing){
		this.addEntity(ValueCode.THING, thing);
	}
	
	addSensor(sensor){
		this.addEntity(ValueCode.SENSOR, sensor);
	}
	
	addObservedProperty(observedProperty){
		this.addEntity(ValueCode.OBSERVED_PROPERTY, observedProperty);
	}
	
	addObservations(observations){
		this.addEntities(ValueCode.OBSERVATIONS, observations);
	}
	
	// Linking between datastreams and FOI is not a part of Sensor Thing standard. However it simplifies the processing of data stream model in WSP
	addFOI(FOI){
		this.addEntity(ValueCode.FEATURE_OF_INTEREST, FOI);
	}
	
	pushObservation(observation){
		this.pushEntity(ValueCode.OBSERVATIONS, observation);
	}
};

function createDatastream(id, name, description, unitOfMeasurement, observationType, observedArea, phenomenonTime, resultTime){
	datastream = new Datastreams(id, name, description, unitOfMeasurement, observationType, observedArea, phenomenonTime, resultTime);
	return datastream;
}

function createFromStreamInfoObject(streamInfo){
	datastream = new Datastreams(streamInfo.id, streamInfo.name, streamInfo.description, streamInfo.unitOfMeasurement, streamInfo.observationType, streamInfo.observedArea, streamInfo.phenomenonTime, streamInfo.resultTime);
	return datastream;
}

function createDummyDatastream(id){
	datastream = new Datastreams(id, "Datastream " + id, "Description of datastream " + id, {name : "Celcius", symbol : "C", desc : "URI"}, ValueCode.OM_CountObservation, null, null, "00:00:00 -- 00-00-0000");
	return datastream;
}

module.exports = createFromStreamInfoObject;
createDatastream.dummy = createDummyDatastream;