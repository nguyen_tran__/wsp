var Generic = require("./Generic");
var ValueCode = require("./ValueCode");

class Sensors extends Generic {
	constructor(id, name, description, encodingType, metadata){
		super(id, ValueCode.SENSORS, [ValueCode.DATASTREAMS]);
		this.name = name;
		this.description = description;
		this.encodingType = ValueCode.getSensorMetadataEncoding(encodingType);
		this.metadata = metadata;
	}
	
	addDatastreams(datastreams){
		this.addEntities(ValueCode.DATASTREAMS, datastreams);
	}
	
	pushDatastream(datastream){
		this.pushEntity(ValueCode.DATASTREAMS, datastream);
	}
};

function createSensor(id, name, description, encodingType, metadata){
	sensor = new Sensors(id, name, description, encodingType, metadata);
	return sensor;
}

function createFromSensorInfoObject(sensorInfo){
	sensor = createSensor(sensorInfo.id, sensorInfo.name, sensorInfo.description, sensorInfo.encodingType, sensorInfo.metadata);
	return sensor;
}

function createDummySensor(id){
	sensor = new Sensors(id, "Sensor " + id, "Description of sensor " + id, "SensorML", {"Key" : "Value"});
	return sensor;
}

module.exports = createFromSensorInfoObject;
createSensor.dummy = createDummySensor;