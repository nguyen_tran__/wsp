var Generic = require("./Generic");
var ValueCode = require("./ValueCode");

class Observations extends Generic {
	constructor(id, phenomenonTime, resultTime, result, resultQuality, validTime, parameters){
		super(id, ValueCode.OBSERVATIONS, [ValueCode.DATASTREAM, ValueCode.FEATURE_OF_INTEREST]);
		this.phenomenonTime = phenomenonTime;
		this.resultTime = resultTime;
		this.result = result;
		this.resultQuality = resultQuality;
		this.validTime = validTime;
		this.parameters = parameters;
	}
	
	addDatastream(datastream){
		this.addEntity(ValueCode.DATASTREAM, datastream);
	}
	
	addFeatureOfInterest(foi){
		this.addEntities(ValueCode.FEATURE_OF_INTEREST, foi);
	}
};

function createObservation(id, phenomenonTime, resultTime, result, resultQuality, validTime, parameters){
	obs = new Observations(id, phenomenonTime, resultTime, result, resultQuality, validTime, parameters);
	return obs;
}

function createDummyObservation(id){
	obs = new Observations(id, "00:00:00 -- 00-00-0000", "00:00:00 -- 00-00-0000", "30", "Quality", null, null);
	return obs;
}

module.exports = createObservation;
createObservation.dummy = createDummyObservation;