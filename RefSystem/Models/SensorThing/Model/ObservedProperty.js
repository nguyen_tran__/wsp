var Generic = require("./Generic");
var ValueCode = require("./ValueCode");

class ObservedProperty extends Generic {
	constructor(id, name, definition, description){
		super(id, ValueCode.OBSERVED_PROPERTIES, [ValueCode.DATASTREAMS]);
		this.name = name;
		this.definition = definition;
		this.description = description;
	}
	
	addDatastreams(datastreams){
		this.addEntities(ValueCode.DATASTREAMS, datastreams);
	}
	
	pushDatastream(datastream){
		this.pushEntity(ValueCode.DATASTREAMS, datastream);
	}
};

function createObservedProperty(id, name, definition, description){
	observedProperty = new ObservedProperty(id, name, definition, description);
	return observedProperty;
}

function createFromInfoObject(PropInfo){
	observedProperty = createObservedProperty(PropInfo.id, PropInfo.name, PropInfo.definition, PropInfo.description);
	return observedProperty;
}

function createDummyObservedProperty(id){
	observedProperty = new ObservedProperty(id, "Observed property " + id, "URI", "This is observed property " + id);
	return observedProperty;
}

module.exports = createFromInfoObject;
createObservedProperty.dummy = createDummyObservedProperty;