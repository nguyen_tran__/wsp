// Import Sensor Thing conceptual classes
var Sensor = require("./Model/Sensor");
var Datastream = require("./Model/Datastream");
var ObservedProperty = require("./Model/ObservedProperty");
var FeatureOfInterest = require("./Model/FeatureOfInterest");
var Observation = require("./Model/Observation");

// Import abstract model manager class from Kernel
var Abstracts = require("./../../Kernel/Abstracts/Abstracts")

// Import constants for datastreams and observation properties
var StreamConsts = require("./d_streams.json")
var PropertyConsts = require("./obs_props.json")

class STModelManager extends Abstracts.AbsModelManager {
	constructor(systemParams, moduleParams, subModules){
		super(systemParams, moduleParams, subModules);
	}
	
	CreateSensorInfo(sensorInfo){
		return Sensor(sensorInfo);
	}
	
	CreateStreamInfo(streamInfo){
		// Check for matching constants in stream info and property info
		function checkConst(constSet, entityInfo, propertyName, replaceAll){
			if(constSet[propertyName].hasOwnProperty(entityInfo[propertyName])){
				if(!replaceAll){
					entityInfo[propertyName] = constSet[propertyName][entityInfo[propertyName]];
					return constSet[propertyName][entityInfo[propertyName]]
				} else {
					var replacingInfo = constSet[propertyName][entityInfo[propertyName]];
					for(var key in replacingInfo){
						if (replacingInfo.hasOwnProperty(key)){
							entityInfo[key] = replacingInfo[key];
						}
					}
				}
			}
		}
		checkConst(StreamConsts, streamInfo, "observationType", false);
		checkConst(StreamConsts, streamInfo, "unitOfMeasurement", false);
		checkConst(PropertyConsts, streamInfo.property, "name", true);
		// console.log(streamInfo);
		
		var stream = Datastream(streamInfo);
		var property = ObservedProperty(streamInfo.property);
		var foi = FeatureOfInterest(streamInfo.FOI);
		stream.addObservedProperty(property);
		stream.addFOI(foi);
		return stream;
	}
	
	CreateObservationInfo(id, phenomenonTime, resultTime, result, resultQuality, validTime, parameters){
		return Observation(id, phenomenonTime, resultTime, result, resultQuality, validTime, parameters)
	}
}
module.exports = STModelManager