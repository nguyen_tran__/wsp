var express = require("express");
var Abstracts = require("./../Abstracts/Abstracts");

class ExpressHTTPRouter extends Abstracts.AbsHTTPRouter{
	constructor(systemParams, moduleParams, subModules){
		super(systemParams, moduleParams, subModules);
		this.SetAttribute("JSONView", "subModules", 0);
		this.router = express.Router();
	}
	
	CreateRouter(){
		this.InitRouter()
		return this.router;
	}
	
	InitRouter(){
		if(this.verb === "GET"){
			var that = this;
			this.router.get(this.route, function(req, res){
				var params = that.ExtractHTTPRequestParams(req);
				var response = that.GenerateHTTPResponse(params);
				that.SendResponse(res, response);
			})
		} else {
			throw "The provided HTTP verb " + this.verb + " is unknown"
		}
	}
}
module.exports = ExpressHTTPRouter;
