var VSs = {};
var Streams = {};
var registries = {
	"Sensors" : VSs,
	"Datastreams" : Streams,
};
module.exports.VSs = VSs;

function addVS(vs){
	// A utility to add an arbitrary entity to an arbitrary list and avoid duplication
	function addToList(key, list, item){
		if(!list.hasOwnProperty(key)){
			list[key] = item
		} else {
			console.log("An item with ID " + key + " already exists in the list. Cancel adding operation");
		}
	}
	
	if(vs.type !== "VS"){
		console.log("The given vs " + vs + " is invalid. Registry adding operation cancelled.");
		return false;
	} else {
		// Add the virtual sensor object into the list
		VSs[vs["sensorID"]] = vs
		addToList(vs["sensorID"], registries["Sensors"], vs)
		
		// Add streams, observed properties and FOI into the list for quick access
		for(var i = 0; i < vs.streams.length; i++){
			addToList(vs.streams[i]["streamID"], registries["Datastreams"], vs.streams[i])
		}
	}
}

function getVS(sensorID){
	if(VSs.hasOwnProperty(sensorID)){
		return VSs[sensorID]
	} else {
		console.log("The given sensor (" + sensorID + ") is not available in the registry. Returning null.")
		return null;
	}
}

function getEntity(type, ID, prop){
	if(registries.hasOwnProperty(type)){
		if(registries[type].hasOwnProperty(ID)){
			entity = registries[type][ID]
			if(!prop){
				return entity;
			} else {
				if(entity.hasOwnProperty(prop)){
					return entity[prop];
				} else if(entity.model.hasOwnProperty(prop)){
					return entity.model[prop];
				} else {
					console.log("The given property ("+ prop +") is not available in the entity " + ID + ". Returning null.")
					return null;
				}
			}
		} else {
			console.log("The given ID (" + ID + ") is not available in the registry. Returning null.")
			return null;
		}
	} else {
		console.log("Unknown entity type (" + type + ") Returning null.")
		return null;
	}
}

function getEntities(type){
	if(registries.hasOwnProperty(type)){
		var entities = []
		for(var key in registries[type]){
			if(registries[type].hasOwnProperty(key)){
				entities.push(registries[type][key]);
			}
		}
		return entities;
	} else {
		console.log("Unknown entity type (" + type + ") Returning null.")
		return null;
	}
}

module.exports.addVS = addVS;
module.exports.getEntity = getEntity;
module.exports.getEntities = getEntities;