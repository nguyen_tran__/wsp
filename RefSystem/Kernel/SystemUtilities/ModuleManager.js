// Load the module of the plugin and 
function LoadModule(moduleInfo, systemParams){
	var moduleName = moduleInfo["moduleName"];
	var modulePath = moduleInfo["modulePath"];
	var moduleParams = moduleInfo["params"];
	var subModuleList = moduleInfo["subModules"];
	
	// Recursively load submodules 
	var subModules = []
	for(var i = 0; i < subModuleList.length; i++){
		subModule = LoadModule(subModuleList[i], {});
		subModules.push(subModule);
	}
	
	var module = new(require(modulePath + "/" + moduleName))(systemParams, moduleParams, subModules);
	console.log("Loaded: " + modulePath + "/" + moduleName);
	return module;
}
module.exports.LoadModule = LoadModule