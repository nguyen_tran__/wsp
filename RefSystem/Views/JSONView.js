var Abstracts = require("./../Kernel/Abstracts/Abstracts")

class JSONView extends Abstracts.AbsView{
	constructor(systemParams, moduleParams, subModules){
		super(systemParams, moduleParams, subModules);
		this.CreateFunctions = {
			"Sensors" : this.CreateSensorJSON,
			"Datastreams" : this.CreateDatastreamJSON,
			"ObservedProperties" : this.CreateObservedProperty,
			"FeaturesOfInterest" : this.CreateFeatureOfInterest,
			"Observations" : this.CreateObservations
		}
	}
	
	CreateView(dataObject, entityType){
		if(!dataObject.length){
			// The given data object contains only one entity
			if(dataObject.hasOwnProperty("type")){
				return this.CreateFunctions[entityType](dataObject);
			} else {
				return dataObject;
			}
		} else {
			// The given data object is an array
			var items = []
			var processedID = {}
			for(var i = 0; i < dataObject.length; i++){
				var item = this.CreateFunctions[entityType](dataObject[i]);
				// console.log(item);
				// Check if the item is available in the data object
				if(!item){
					continue;
				}
				var itemID = item["@iot.id"];
				if(!itemID){
					itemID = item.observationID;
				}
				// Check and avoid adding duplication to the list
				if(!processedID.hasOwnProperty(itemID)){
					processedID[itemID] = itemID;
					items.push(item);
				}
			}
			return {
				"@iot.count" : items.length,
				"value" : items
			}
		}
	}
	
	CreateSensorJSON(vs){
		return vs.sensorInfo;
	}
	
	CreateDatastreamJSON(datastream){
		return datastream.streamInfo;
	}
	
	CreateObservedProperty(datastream){
		return datastream.streamInfo.ObservedProperty;
	}
	
	CreateFeatureOfInterest(datastream){
		return datastream.streamInfo.FeatureOfInterest;
	}
	
	// Two the input of this function is either a datastream, in case the URL was for all observations
	// or an observation object, in case the URL was for the observation of a specific object
	CreateObservations(entity){
		if(entity.type === "VSSTREAM"){
			try{
				return entity.Observations
			} catch (err) {
				return null
			}
		} else if(entity.type === "OBSERVATION"){
			return entity.model;
		} else {
			return null
		}
	}
}
module.exports = JSONView;
